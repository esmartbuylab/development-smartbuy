FROM java:8
FROM maven:alpine

WORKDIR /app

COPY . /app

RUN mvn -v
RUN mvn clean install -DskipTests
EXPOSE 8080
LABEL maintainer=“gitlearninggit@gmail.com”
ADD ./target/smartbuy-dev-0.0.1-SNAPSHOT.jar smartbuy-dev-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","smartbuy-dev-0.0.1-SNAPSHOT.jar"]