package com.ameri100.smartbuydev.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ameri100.smartbuydev.repository.UserRepository;

@RestController
@RequestMapping("/api")
public class UserController {
    @Autowired
    private UserRepository userRepository;
    @RequestMapping("/")
	public String index() {
		return "Greetings from Spring Boot!";
	}

    
}
